package by.iba.yahorfilipchyk.web.employees.dao.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP
 * DAO for accessing Employee entities using jdbc
 */
public class JdbcEmployeeDAO extends JdbcDAO implements OrgEntityDAO<Employee> {

	@Override
	public Employee find(int key) {
		return find(key, true);
	}
	
	public Employee find(int key, boolean withRefs) {
		Employee result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM employee WHERE employee.id = " + key;
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				result = new Employee();
				result.setId(resultSet.getInt("id"));
				result.setFullName(resultSet.getString("fullName"));
				result.setPhone(resultSet.getString("phone"));
				result.setPosition(resultSet.getString("position"));
				result.setStartDate(resultSet.getDate("startDate"));
				result.setEndDate(resultSet.getDate("endDate"));
				if (withRefs) {
					int deptId = resultSet.getInt("deptId");
					result.setDepartment(new JdbcDepartmentDAO().find(deptId, false));
				}
				System.out.println("from find(id): " + result);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}
	
	@Override
	public List<Employee> findCommon(Employee sample) {
		return findCommon(sample, true);
	}

	public List<Employee> findCommon(Employee sample, boolean withRefs) {
		List<Employee> result = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			System.out.println("Connection established");
			Statement statement = connection.createStatement();
			System.out.println("Satement created");
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM employee");
			boolean firstClauseAdded = false;
			if (sample.getFullName() != null && !sample.getFullName().isEmpty()) {
				query.append(" WHERE UPPER(employee.fullName) LIKE '%" + sample.getFullName().toUpperCase() + "%'");
				firstClauseAdded = true;
			}
			if (sample.getDepartment() != null && !sample.getDepartment().getDeptName().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.deptId = " + new JdbcDepartmentDAO().find(sample.getDepartment(), false).getId());
			}
			if (sample.getPhone() != null && !sample.getPhone().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("UPPER(employee.phone) LIKE '%" + sample.getPhone().toUpperCase() + "%'");
			}
			if (sample.getPosition() != null && !sample.getPosition().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("UPPER(employee.position) LIKE '%" + sample.getPosition().toUpperCase() + "%'");
			}
			if (sample.getStartDate() != null) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.startDate = " + "'" + sample.getStartDate().toString() + "'");
			}
			if (sample.getEndDate() != null) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.endDate = " + "'" + sample.getEndDate().toString() + "'");
			}
			System.out.println(query.toString());
			// no fields specified to search by in the sample object
			if (!firstClauseAdded) {
				return result;
			}
			System.out.println(query.toString());
			ResultSet resultSet = statement.executeQuery(query.toString());
			while (resultSet.next()) {
				Employee empl = new Employee();
				empl.setId(resultSet.getInt("id"));
				empl.setFullName(resultSet.getString("fullName"));
				empl.setPhone(resultSet.getString("phone"));
				empl.setPosition(resultSet.getString("position"));
				empl.setStartDate(resultSet.getDate("startDate"));
				empl.setEndDate(resultSet.getDate("endDate"));
				if (withRefs) {
					int deptId = resultSet.getInt("deptId");
					empl.setDepartment(new JdbcDepartmentDAO().find(deptId, false));
				}
				result.add(empl);
			}
		} catch (SQLException ex) { 
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}

	@Override
	public List<Employee> getAll() {
		List<Employee> employees = new ArrayList<Employee>();
		Connection connection = null;
        try {
        	connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT id, fullName, startDate, endDate, phone, position, deptName "
                    + "FROM employee "
                    + "LEFT JOIN department "
                    + "ON department.deptId = employee.deptId";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                Employee employee = new Employee();
				employee.setId(result.getInt("id"));
				employee.setFullName(result.getString("fullName"));
				// setting dates
				//System.out.println(result.getString("start_date"));
				Date date = result.getDate("startDate");
				employee.setStartDate(date);
				date = result.getDate("endDate");
				employee.setEndDate(date);
                employee.setPhone(result.getString("phone"));
                employee.setPosition(result.getString("position"));
                Department department = new Department();
                department.setDeptName(result.getString("deptName"));
                employee.setDepartment(department);
                
                employees.add(employee);
            }
            statement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
        return employees;
	}

	@Override
	public void save(Employee entity) {
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			if (entity.getDepartment() != null && entity.getDepartment().getId() == 0) {
				JdbcDepartmentDAO deptDAO = new JdbcDepartmentDAO();
				deptDAO.save(entity.getDepartment());
				entity.setDepartment(deptDAO.find(entity.getDepartment(), false));
			}
			String startDate = null;
			if (entity.getStartDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				startDate = format.format(entity.getStartDate());
			}
			System.out.println(startDate);
			String endDate = null;
			if (entity.getEndDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				startDate = format.format(entity.getEndDate());
			}
			Formatter queryFormatter = new Formatter();
			queryFormatter = queryFormatter.format(
					"INSERT INTO employee ("
					+ "fullName, "
					+ "phone, "
					+ "position, "
					+ "startDate, "
					+ "endDate, "
					+ "deptId) "
					+ "VALUES (%s, %s, %s,  %s, %s, %d)", 
					entity.getFullName() == null ? null : "'" + entity.getFullName() + "'", 
					entity.getPhone() == null ? null : "'" + entity.getPhone() + "'",
					entity.getPosition() == null ? null : "'" + entity.getPosition() + "'", 
					startDate == null ? null : "'" + startDate + "'", 
					endDate == null ? null : "'" + endDate + "'", 
					entity.getDepartment() == null ? null : entity.getDepartment().getId());
			System.out.println(queryFormatter.toString());
			statement.executeUpdate(queryFormatter.toString());
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean update(Employee entity) {
		boolean updated = false;
		Connection connection = null;
		try {
			connection = getConnection();
			System.out.println("From update employee: " + entity);
			if (entity.getDepartment() != null && entity.getDepartment().getId() == 0) {
				JdbcDepartmentDAO deptDAO = new JdbcDepartmentDAO();
				deptDAO.save(entity.getDepartment());
				entity.setDepartment(deptDAO.find(entity.getDepartment(), false));
			}
			Statement statement = connection.createStatement();
			Formatter queryFormatter = new Formatter();
			queryFormatter = queryFormatter.format(
					"UPDATE employee SET "
					+ "fullName=%s,"
					+ "phone=%s,"
					+ "position=%s,"
					+ "startDate=%s,"
					+ "endDate=%s,"
					+ "deptId=%d"
					+ " WHERE employee.id = %d",
					entity.getFullName() == null ? null : "'" + entity.getFullName() + "'",
					entity.getPhone() == null ? null : "'" + entity.getPhone() + "'",
					entity.getPosition() == null ? null : "'" + entity.getPosition() + "'",
					entity.getStartDate() == null ? null : "'" + entity.getStartDate() + "'",
					entity.getEndDate() == null ? null : "'" + entity.getEndDate() + "'",
					entity.getDepartment() == null ? null : entity.getDepartment().getId(),
					entity.getId());
			statement.executeUpdate(queryFormatter.toString());
			updated = true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return updated;
	}

	@Override
	public boolean delete(Employee entity) {
		boolean deleted = false;
		if (entity.getId() == 0) {
			entity = find(entity, false);
		}
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			String query = "DELETE FROM employee WHERE employee.id = " + entity.getId();
			statement.executeUpdate(query);
			deleted = true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}		
		return deleted;
	}

	@Override
	public Employee find(Employee sample) {
		return find(sample, true);
	}
	
	public Employee find(Employee sample, boolean withRefs) {
		Employee result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			System.out.println("Connection established");
			Statement statement = connection.createStatement();
			System.out.println("Satement created");
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM employee");
			boolean firstClauseAdded = false;
			if (sample.getFullName() != null && !sample.getFullName().isEmpty()) {
				query.append(" WHERE employee.fullName = '" + sample.getFullName() + "'");
				firstClauseAdded = true;
			}
			if (sample.getDepartment() != null && !sample.getDepartment().getDeptName().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.deptId = " + new JdbcDepartmentDAO().find(sample.getDepartment(), false).getId());
			}
			if (sample.getPhone() != null && !sample.getPhone().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.phone = '" + sample.getPhone() + "'");
			}
			if (sample.getPosition() != null && !sample.getPosition().isEmpty()) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.position = '" + sample.getPosition() + "'");
			}
			if (sample.getStartDate() != null) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.startDate = '" + sample.getStartDate().toString() + "'");
			}
			if (sample.getEndDate() != null) {
				if (!firstClauseAdded) {
					query.append(" WHERE ");
					firstClauseAdded = true;
				} else {
					query.append(" AND ");
				}
				query.append("employee.endDate = '" + sample.getEndDate().toString() + "'");
			}
			System.out.println(query.toString());
			// no fields specified to search by in the sample object
			if (!firstClauseAdded) {
				return result;
			}
			System.out.println(query.toString());
			ResultSet resultSet = statement.executeQuery(query.toString());
			while (resultSet.next()) {
				Employee empl = new Employee();
				empl.setId(resultSet.getInt("id"));
				empl.setFullName(resultSet.getString("fullName"));
				empl.setPhone(resultSet.getString("phone"));
				empl.setPosition(resultSet.getString("position"));
				empl.setStartDate(resultSet.getDate("startDate"));
				empl.setEndDate(resultSet.getDate("endDate"));
				if (withRefs) {
					int deptId = resultSet.getInt("deptId");
					empl.setDepartment(new JdbcDepartmentDAO().find(deptId, false));
				}
				result = empl;
			}
		} catch (SQLException ex) { 
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}


}

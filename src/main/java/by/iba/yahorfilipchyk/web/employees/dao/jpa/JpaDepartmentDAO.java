package by.iba.yahorfilipchyk.web.employees.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;

/**
 * 
 * @author Filipchyk_YP
 * DAO for accessing Department entities using jpa
 */
public class JpaDepartmentDAO extends JpaDAO implements OrgEntityDAO<Department> {

	@Override
	public Department find(int key) {
		Department result = null;
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			result = em.find(Department.class, key);
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}

	public List<Department> findCommon(Department sample) {
		List<Department> result = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT d FROM Department d");
		boolean firstClauseAdded = false;
		if (sample.getDeptName() != null) {
			query.append(" WHERE UPPER(d.deptName) LIKE '%" + sample.getDeptName().toUpperCase() + "%'");
			firstClauseAdded = true;
		}
		// no fields specified to search by in the sample object
		if (!firstClauseAdded) {
			return result;
		}
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			System.out.println(query.toString());
			Query q = em.createQuery(query.toString());
			result = (List<Department>) q.getResultList();
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}

	public List<Department> getAll() {		
		return (List<Department>) getAll(Department.class);
	}

	public void save(Department entity) {
		persist(entity);	
	}

	@Override
	public boolean update(Department entity) {
		return super.refresh(entity);
	}

	@Override
	public boolean delete(Department entity) {
		return remove(entity);		
	}

	@Override
	public Department find(Department sample) {
		Department result = null;
		StringBuilder query = new StringBuilder();
		query.append("SELECT d FROM Department d");
		boolean firstClauseAdded = false;
		if (sample.getDeptName() != null) {
			query.append(" WHERE d.deptName = '" + sample.getDeptName() + "'");
			firstClauseAdded = true;
		}
		// no fields specified to search by in the sample object
		if (!firstClauseAdded) {
			return result;
		}
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			System.out.println(query.toString());
			Query q = em.createQuery(query.toString());
			result = (Department) q.getSingleResult();
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}

}

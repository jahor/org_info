package by.iba.yahorfilipchyk.web.employees.serv;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;
import by.iba.yahorfilipchyk.web.employees.management.ConnectionManager;

public class DataConverterUtil {

	public static Employee getEmployeeFromGet(String prefix, ConnectionManager manager, HttpServletRequest request) {
		
		Employee sample = new Employee();
		sample.setFullName(request.getParameter(prefix + "name"));
		sample.setPhone(request.getParameter(prefix + "phone"));
		
		// setting department
		Department dept = new Department();
		dept.setDeptName(request.getParameter(prefix + "dept"));
		OrgEntityDAO<Department> deptFindDAO = manager.createDepartmentDAO();
		Department foundDept = deptFindDAO.find(dept);
		if (foundDept != null) {
			dept = foundDept;
		}
		sample.setDepartment(dept);
		
		sample.setPosition(request.getParameter(prefix + "position"));
		String startDate = request.getParameter(prefix + "start_date");
		if (startDate != null && !startDate.isEmpty()) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date parsed = format.parse(startDate);
				Date sqlDate = new Date(parsed.getTime());
				sample.setStartDate(sqlDate);
			} catch (ParseException ex) {
				ex.printStackTrace();
			}
		}
		String endDate = request.getParameter(prefix + "end_date");
		if (endDate != null && !endDate.isEmpty()) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date parsed = format.parse(endDate);
				Date sqlDate = new Date(parsed.getTime());
				sample.setEndDate(sqlDate);
			} catch (ParseException ex) {
				ex.printStackTrace();
			}
		}
		return sample;
	}
	
public static Department getDepartmentFromGet(String prefix, ConnectionManager manager, HttpServletRequest request) {
		Department sample = new Department();
		sample.setDeptName(request.getParameter(prefix + "dept_name"));
		return sample;
	}
}

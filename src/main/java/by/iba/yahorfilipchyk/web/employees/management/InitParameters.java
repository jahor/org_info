package by.iba.yahorfilipchyk.web.employees.management;

import java.util.Map;

public class InitParameters {

	private String driveType;
	private Map<String, String> parameters;
	
	public InitParameters() {
	}
	
	public InitParameters(String type, Map<String, String> params) {
		this.driveType = type;
		this.parameters = params;
	}
	
	public String getDriveType() {
		return driveType;
	}
	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}
	public Map<String, String> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "InitParameters [driveType=" + driveType + ", parameters="
				+ parameters + "]";
	}
}

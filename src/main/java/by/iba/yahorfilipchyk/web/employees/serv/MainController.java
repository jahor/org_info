package by.iba.yahorfilipchyk.web.employees.serv;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.betwixt.io.BeanWriter;
import org.xml.sax.SAXException;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;
import by.iba.yahorfilipchyk.web.employees.management.ConnectionManager;

/**
 * Main controller of user's requests. Processes GET method.
 */
public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Bean, injected by spring
	 */
	// @Autowired
	private ConnectionManager manager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// ApplicationContext context =
		// WebApplicationContextUtils.getRequiredWebApplicationContext((getServletContext()));
		// manager = (ConnectionManager) context.getBean("manager");
		
		String pageToShow = request.getParameter("page");
		if (pageToShow != null) {
			show(pageToShow, request, response);
			return;
		}
		
		request.getRequestDispatcher("error.jsp").forward(request,
				response);
	}
	
	private void update(String whoToUpdate, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String showingPage = "error";

		switch (whoToUpdate.toLowerCase()) {
		case "employee":
			showingPage = "employees";
			
			// creating of data access object and retrieving of information from
			// datastore
			OrgEntityDAO<Employee> emplDAO = manager.createEmployeeDAO();
			System.out.println("id: " + request.getParameter("empl_id"));
			Employee sample = emplDAO.find(Integer.parseInt(request.getParameter("empl_id")));
			System.out.println(sample);
			if (sample != null) {
				Employee emplToUpdate = DataConverterUtil.getEmployeeFromGet("", manager, request);
				emplToUpdate.setId(sample.getId());
				emplDAO.update(emplToUpdate);
			}
			request.getSession().setAttribute("employees", emplDAO.getAll());
			request.getSession().setAttribute("departments", manager.createDepartmentDAO().getAll());
			break;
		case "department":
			showingPage = "departments";
			OrgEntityDAO<Department> deptDAO = manager.createDepartmentDAO();
			Department sampleDept = deptDAO.find(Integer.parseInt(request.getParameter("dept_id")));
			System.out.println(sampleDept);
			if (sampleDept != null) {
				Department deptToUpdate = DataConverterUtil.getDepartmentFromGet("", manager, request);
				System.out.println(deptToUpdate);
				deptToUpdate.setId(sampleDept.getId());
				System.out.println(deptToUpdate);
				deptDAO.update(deptToUpdate);
			}
			request.getSession().setAttribute("departments", deptDAO.getAll());
			break;
		default:
			break;
		}
		request.getRequestDispatcher(showingPage + ".jsp").forward(request,
				response);
	}
	
	private void delete(String whoToDelete, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String showingPage = "error";

		switch (whoToDelete.toLowerCase()) {
		case "employee":
			showingPage = "employees";
			// creating of data access object and retrieving of information from
			// datastore
			Employee sample = DataConverterUtil.getEmployeeFromGet("", manager, request);
			OrgEntityDAO<Employee> emplDAO = manager.createEmployeeDAO();			
			System.out.println(sample);
			Employee employeeToDelete = emplDAO.find(sample);
			System.out.println(employeeToDelete);
			List<Employee> allEmployees = (List<Employee>) request.getSession().getAttribute("employees");
			if (employeeToDelete != null) {
				if (emplDAO.delete(employeeToDelete)) {
					allEmployees.remove(employeeToDelete);
				}
			}
			request.getSession().setAttribute("employees", allEmployees);
			request.getSession().setAttribute("departments", manager.createDepartmentDAO().getAll());
			break;
		case "department":
			showingPage = "departments";
			Department sampleDept = DataConverterUtil.getDepartmentFromGet("", manager, request);
			OrgEntityDAO<Department> deptDAO = manager.createDepartmentDAO();			
			System.out.println(sampleDept);
			Department deptToDelete = deptDAO.find(sampleDept);
			System.out.println(deptToDelete);
			List<Department> departments = (List<Department>) request.getSession().getAttribute("departments");
			if (deptToDelete != null) {
				if (deptDAO.delete(deptToDelete)) {
					departments.remove(deptToDelete);
				}
			}
			request.getSession().setAttribute("departments", departments);
			break;
		default:
			break;
		}
		request.getRequestDispatcher(showingPage + ".jsp").forward(request,
				response);
	}

	private void addEntity(String whoToAdd,	HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String showingPage = "error";

		switch (whoToAdd.toLowerCase()) {
		case "employee":
			showingPage = "employees";
			// creating of data access object and retrieving of information from
			// datastore
			Employee newEmpl = DataConverterUtil.getEmployeeFromGet("", manager, request);			
			OrgEntityDAO<Employee> emplDAO = manager.createEmployeeDAO();			
			System.out.println(newEmpl);
			emplDAO.save(newEmpl);
			List<Employee> employees = emplDAO.getAll();
			request.getSession().setAttribute("employees", employees);
			request.getSession().setAttribute("departments", manager.createDepartmentDAO().getAll());
			break;
		case "department":
			showingPage = "departments";
			Department newDept = DataConverterUtil.getDepartmentFromGet("", manager, request);			
			OrgEntityDAO<Department> deptDAO = manager.createDepartmentDAO();			
			System.out.println(newDept);
			deptDAO.save(newDept);
			List<Department> departments = deptDAO.getAll();
			request.getSession().setAttribute("departments", departments);
			break;
		default:
			break;
		}
		request.getRequestDispatcher(showingPage + ".jsp").forward(request,
				response);
	}
	
	private void search(String whoToSearch, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String showingPage = "error";

		switch (whoToSearch.toLowerCase()) {
		case "employee":
			showingPage = "employees";
			// creating of data access object and retrieving of information from
			// datastore			
			Employee sample = DataConverterUtil.getEmployeeFromGet("", manager, request);
			System.out.println("from main:search: " + sample);
			OrgEntityDAO<Employee> emplDAO = manager.createEmployeeDAO();			
			System.out.println("from main:search: " + sample);
			List<Employee> employees = emplDAO.findCommon(sample);
			request.getSession().setAttribute("employees", employees);
			request.getSession().setAttribute("departments", manager.createDepartmentDAO().getAll());
			break;
		case "department":
			showingPage = "departments";
			Department sampleDept = DataConverterUtil.getDepartmentFromGet("", manager, request);
			OrgEntityDAO<Department> deptDAO = manager.createDepartmentDAO();			
			System.out.println(sampleDept);
			List<Department> departments = deptDAO.findCommon(sampleDept);
			request.getSession().setAttribute("departments", departments);
			break;
		default:
			break;
		}
		request.getRequestDispatcher(showingPage + ".jsp").forward(request,
				response);
	}

	private void show(String pageToShow, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String showingPage = "error";

		switch (pageToShow.toLowerCase()) {
		case "employees":
			showingPage = "employees";
			// creating of data access object and retrieving of information from
			// datastore
			OrgEntityDAO<Employee> emplDAO = manager.createEmployeeDAO();
			List<Employee> employees = emplDAO.getAll();
			for (Employee empl : employees) {
//				System.out.println(empl);
						        
			}
			request.getSession().setAttribute("departments", manager.createDepartmentDAO().getAll());
			request.getSession().setAttribute("employees", employees);
			break;
		case "departments":
			showingPage = "departments";
			OrgEntityDAO<Department> deptDAO = manager.createDepartmentDAO();
			List<Department> departments = deptDAO.getAll();
//			for (Department dept : departments) {
//				System.out.println(dept);
//			}
			request.getSession().setAttribute("departments", departments);
			break;
		case "init":
			showingPage = "init";
			// initializing of datastore (only with jpa connection manager)
			OrgEntityDAO<Employee> dao = manager.createEmployeeDAO();
			init(dao);
			break;
		default:
			break;
		}
		request.getRequestDispatcher(showingPage + ".jsp").forward(request,
				response);
	}

	private static void init(OrgEntityDAO<Employee> dao) {
		Department d1 = new Department();
		d1.setDeptName("Web developing");
		Department d2 = new Department();
		d2.setDeptName("HR");
		Department d3 = new Department();
		d3.setDeptName("System developing");
		List<Employee> employeesDept1 = new ArrayList<Employee>();
		List<Employee> employeesDept2 = new ArrayList<Employee>();
		List<Employee> employeesDept3 = new ArrayList<Employee>();

		Employee e1 = new Employee();
		e1.setDepartment(d1);
		e1.setFullName("John Smith");
		e1.setStartDate(new Date(System.currentTimeMillis()));
		e1.setEndDate(new Date(System.currentTimeMillis() + 100000));
		e1.setPhone("+123456789");
		e1.setPosition("Java developer");
		employeesDept1.add(e1);

		Employee e2 = new Employee();
		e2.setDepartment(d2);
		e2.setFullName("Seth Mac'farlein");
		e2.setStartDate(new Date(System.currentTimeMillis() - 14235));
		e2.setPhone("+1224545789");
		e2.setPosition("Recuter");
		employeesDept2.add(e2);

		Employee e3 = new Employee();
		e3.setDepartment(d1);
		e3.setFullName("John Cale");
		e3.setStartDate(new Date(System.currentTimeMillis() - 13435));
		e3.setPhone("+525324545789");
		e3.setPosition("Architect");
		employeesDept1.add(e3);

		Employee e4 = new Employee();
		e4.setDepartment(d3);
		e4.setFullName("Mathue Hiffi");
		e4.setStartDate(new Date(System.currentTimeMillis() - 2355));
		e4.setPhone("+1224024989");
		e4.setPosition("C++ Programmer");
		employeesDept3.add(e4);

		Employee e5 = new Employee();
		e5.setDepartment(d1);
		e5.setFullName("Math Shadows");
		e5.setStartDate(new Date(System.currentTimeMillis() - 990));
		e5.setPhone("+1235135351");
		e5.setPosition("Senior Java developer");
		employeesDept1.add(e5);

		d1.setEmployees(employeesDept1);
		d2.setEmployees(employeesDept2);
		d3.setEmployees(employeesDept3);

		dao.save(e1);
		dao.save(e2);
		dao.save(e3);
		dao.save(e4);
		dao.save(e5);
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		manager = (ConnectionManager) config.getServletContext().getAttribute(
				"manager");
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String whoToAdd = request.getParameter("add");
		if (whoToAdd != null) {
			addEntity(whoToAdd, request, response);
			return;
		}
		String whoToSearch = request.getParameter("search");
		if (whoToSearch != null) {
			search(whoToSearch, request, response);
			return;
		}
		String whoToDelete = request.getParameter("delete");
		if (whoToDelete != null) {
			delete(whoToDelete, request, response);
			return;
		}
		String whoToUpdate = request.getParameter("update");
		if (whoToUpdate != null) {
			update(whoToUpdate, request, response);
			return;
		}		
		// ajax request handling
		String whoToGet = request.getParameter("get");
		int entityId = Integer.parseInt(request.getParameter("id"));
		if (whoToGet != null) {
			getEntity(whoToGet, entityId, request, response);
			return;
		}
		request.getRequestDispatcher("error.jsp").forward(request,
				response);
	}
	
	private void getEntity(String whoToGet, int id, HttpServletRequest request, HttpServletResponse response) {
		StringWriter out = new StringWriter();
		out.write("<?xml version=\"1.0\" ?>\n");
		BeanWriter beanWriter = new BeanWriter(out);
		beanWriter.getXMLIntrospector().getConfiguration().setAttributesForPrimitives(false);
        beanWriter.getBindingConfiguration().setMapIDs(false);
        beanWriter.setEndTagForEmptyElement(true);
        beanWriter.enablePrettyPrint();          
		switch (whoToGet) {
		case "employee":
			Employee empl = manager.createEmployeeDAO().find(id);
			// remove relations to prevent recursion
			empl.getDepartment().setEmployees(null);
			try {
				beanWriter.write("employee", empl);
		        System.out.println(out.toString());     
		        response.setContentType("application/xml");
		        response.getWriter().write(out.toString());
		        response.getWriter().flush();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			try {
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			break;
		case "department":
			Department dept = manager.createDepartmentDAO().find(id);
			// remove relations to prevent recursion
			for (Employee employee : dept.getEmployees()) {
				employee.setDepartment(null);
			}
//			dept.setEmployees(null);
			try {
				beanWriter.write("department", dept);
		        System.out.println(out.toString());     
		        response.setContentType("application/xml");
		        response.getWriter().write(out.toString());
		        response.getWriter().flush();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			try {
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			break;
		}
	}

}

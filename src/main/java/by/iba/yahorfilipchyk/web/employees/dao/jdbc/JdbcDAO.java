package by.iba.yahorfilipchyk.web.employees.dao.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import by.iba.yahorfilipchyk.web.employees.management.ConnectionManagerJDBC;

/**
 * 
 * @author Filipchyk_YP 
 * Parent class for all DAO classes accessing datastore
 * using jdbc. Encapsulates DataSource object and provides connections
 * for accessing data.
 */
public class JdbcDAO {

	/**
	 * 
	 * @return Connection to DataSource
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return ConnectionManagerJDBC.getConnection();
	}
}

package by.iba.yahorfilipchyk.web.employees.management;

import java.util.Map;

/**
 * 
 * @author Filipchyk_YP
 * Factory for managers classes.
 */
public class ManagersFactory {

	public static ConnectionManager createManager(InitParameters params) {
		System.out.println(params);
		switch (params.getDriveType()) {
		case "type_jdbc":			
			Map<String, String> jdbcParams = params.getParameters();
			return new ConnectionManagerJDBC(
					jdbcParams.get("driver"), 
					jdbcParams.get("url"),
					jdbcParams.get("username"),
					jdbcParams.get("password")
			);
		case "type_jpa":
			Map<String, String> jpaParams = params.getParameters();
			return new ConnectionManagerJPA(
					jpaParams.get("unit")
			);
		default:
			return null;
		}
	}
}
package by.iba.yahorfilipchyk.web.employees.dao.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP
 * DAO for accessing Department entities using jdbc
 */
public class JdbcDepartmentDAO extends JdbcDAO implements OrgEntityDAO<Department> {

	/**
	 * @see by.iba.yahorfilipchyk.web.employees.dao.DepartmentDAO#find
	 */
	public Department find(int key) {
		return find(key, true);
	}
	
	/**
	 * Finds entity by the key with or without references.
	 * @param key 
	 * @param withRefs 
	 * @return
	 */
	public Department find(int key, boolean withRefs) {
		Department result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM department WHERE department.deptId = " + key;
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				result = new Department();
				result.setId(resultSet.getInt("deptId"));
				result.setDeptName(resultSet.getString("deptName"));
				// finding references if necessary
				if (withRefs) {
					Employee sample = new Employee();
					sample.setDepartment(result);
					List<Employee> employees = new JdbcEmployeeDAO()
							.findCommon(sample, false);
					result.setEmployees(employees);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}
	
	@Override
	public List<Department> findCommon(Department sample) {
		return findCommon(sample, true);
	}

	public List<Department> findCommon(Department sample, boolean withRefs) {
		List<Department> result = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM department");
			boolean firstClauseAdded = false;
			if (sample.getDeptName() != null) {
				query.append(" WHERE UPPER(department.deptName) LIKE '%" + sample.getDeptName().toUpperCase() + "%'");
				firstClauseAdded = true;
			}
			// no fields specified to search by in the sample object
			if (!firstClauseAdded) {
				return result;
			}
			ResultSet resultSet = statement.executeQuery(query.toString());
			while (resultSet.next()) {
				Department dept = new Department();
				dept.setId(resultSet.getInt("deptId"));
				dept.setDeptName(resultSet.getString("deptName"));
				if (withRefs) {
					Employee sampleEmpl = new Employee();
					sampleEmpl.setDepartment(dept);
					List<Employee> employees = new JdbcEmployeeDAO()
							.findCommon(sampleEmpl, false);
					dept.setEmployees(employees);
				}
				result.add(dept);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException  ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}

	@Override
	public List<Department> getAll() {
		List<Department> departments = new ArrayList<Department>();
		Connection connection = null;
        try {
        	connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT department.deptId, deptName, id, fullName "
            		+ "FROM department "
            		+ "LEFT JOIN employee "
            		+ "ON employee.deptId = department.deptId";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
            	String deptName = result.getString("deptName");
            	String employeeName = result.getString("fullName");
            	int emplID = result.getInt("id");
            	int deptID = result.getInt("deptId");
            	boolean hasDept = false;
            	for (int i = 0 ; i < departments.size(); i++) {
            		Department dept = departments.get(i);
            		if (deptName.equals(dept.getDeptName())) {
            			hasDept = true;
            			break;
            		}
            	}
            	if (!hasDept) {
            		Department newDept = new Department();
            		newDept.setDeptName(deptName);
            		newDept.setId(deptID);
            		newDept.setEmployees(new ArrayList<Employee>());
            		departments.add(newDept);
            	}
				for (int i = 0; i < departments.size(); i++) {
					Department dept = departments.get(i);
					if (dept.getId() == deptID) {
						Employee newEmpl = new Employee();
						newEmpl.setId(emplID);
						newEmpl.setFullName(employeeName);
						dept.getEmployees().add(newEmpl);
					}
				}

			}
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
        }
        return departments;
	}

	@Override
	public void save(Department entity) {
		Department dept = find(entity, false);
		System.out.println("From save dept: " + dept);
		if (dept != null && dept.getDeptName() != null && dept.getDeptName().equals(entity.getDeptName())) {
			return;
		}
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			Formatter queryFormatter = new Formatter();
			queryFormatter = queryFormatter.format(
					"INSERT INTO department ("
					+ "deptName) "
					+ "VALUES (%s)", 
					entity.getDeptName() == null ? null : "'" + entity.getDeptName() + "'");
			statement.executeUpdate(queryFormatter.toString());
			System.out.println(queryFormatter.toString());
			if (entity.getEmployees() != null) {
				entity = find(entity, true);
				OrgEntityDAO<Employee> emplDAO = new JdbcEmployeeDAO();
				for (Employee empl : entity.getEmployees()) {
					empl.setDepartment(entity);
					emplDAO.save(empl);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean update(Department entity) {
		boolean updated = false;
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			Formatter queryFormatter = new Formatter();
			queryFormatter = queryFormatter.format(
					"UPDATE department SET "
					+ "deptName=%s WHERE department.deptId = %d",
					entity.getDeptName() == null ? null : "'" + entity.getDeptName() + "'",
					entity.getId());
			statement.executeUpdate(queryFormatter.toString());
			updated = true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return updated;
	}

	@Override
	public boolean delete(Department entity) {
		boolean deleted = false;
		entity = find(entity, true);
		List<Employee> employees = entity.getEmployees();
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			String query = "DELETE FROM department WHERE department.deptId = " + entity.getId();
			statement.executeUpdate(query);
			deleted = true;
			JdbcEmployeeDAO emplDAO = new JdbcEmployeeDAO();
			for (Employee empl : employees) {
				emplDAO.delete(empl);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return deleted;
	}

	@Override
	public Department find(Department sample) {
		return find(sample, true);
	}
	
	public Department find(Department sample, boolean withRefs) {
		Department result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM department");
			boolean firstClauseAdded = false;
			if (sample.getDeptName() != null) {
				query.append(" WHERE department.deptName = '" + sample.getDeptName() + "'");
				firstClauseAdded = true;
			}
			// no fields specified to search by in the sample object
			if (!firstClauseAdded) {
				return result;
			}
			System.out.println(query.toString());
			ResultSet resultSet = statement.executeQuery(query.toString());
			while (resultSet.next()) {
				result = new Department();
				result.setId(resultSet.getInt("deptId"));
				result.setDeptName(resultSet.getString("deptName"));
				if (withRefs) {
					Employee sampleEmpl = new Employee();
					sampleEmpl.setDepartment(result);
					List<Employee> employees = new JdbcEmployeeDAO()
							.findCommon(sampleEmpl, false);
					result.setEmployees(employees);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException  ex) {
					ex.printStackTrace();
				}
			}
		}
		return result;
	}


}

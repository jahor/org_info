package by.iba.yahorfilipchyk.web.employees.management;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.dao.jpa.JpaDepartmentDAO;
import by.iba.yahorfilipchyk.web.employees.dao.jpa.JpaEmployeeDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP
 * Connection manager for accessing the database using jpa.
 */
public class ConnectionManagerJPA implements ConnectionManager {

	private static EntityManagerFactory emf;
	
	public ConnectionManagerJPA(String unit) {
		emf = Persistence.createEntityManagerFactory(unit);
	}
	
	public static EntityManager createEntityManager() {
		return emf.createEntityManager();
	}
	
	/**
	 * Closes Entity manager factory.
	 */
	@Override
	public void close() {
		System.out.println("Closing entity manager factory");
		if (emf != null) {
			emf.close();
		}
	}

	@Override
	public OrgEntityDAO<Department> createDepartmentDAO() {
		return new JpaDepartmentDAO();
	}

	@Override
	public OrgEntityDAO<Employee> createEmployeeDAO() {
		return new JpaEmployeeDAO();
	}

}

package by.iba.yahorfilipchyk.web.employees.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP
 * DAO for accessing Employee entities using jpa
 */
public class JpaEmployeeDAO extends JpaDAO implements OrgEntityDAO<Employee> {

	@Override
	public Employee find(int key) {
		Employee result = null;
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			result = em.find(Employee.class, key);
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}

	public List<Employee> findCommon(Employee sample) {
		List<Employee> result = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT e FROM Employee e");
		boolean firstClauseAdded = false;
		if (sample.getFullName() != null && !sample.getFullName().isEmpty()) {
			query.append(" WHERE UPPER(e.fullName) LIKE '%" + sample.getFullName().toUpperCase() + "%'");
			firstClauseAdded = true;
		}
		if (sample.getDepartment() != null && !sample.getDepartment().getDeptName().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("UPPER(e.department.deptName) LIKE '%" + sample.getDepartment().getDeptName().toUpperCase() + "%'");
		}
		if (sample.getPhone() != null && !sample.getPhone().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("UPPER(e.phone) LIKE '%" + sample.getPhone().toUpperCase() + "%'");
		}
		if (sample.getPosition() != null && !sample.getPosition().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("UPPER(e.position) LIKE '%" + sample.getPosition().toUpperCase() + "%'");
		}
		if (sample.getStartDate() != null) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.startDate = " + "'" + sample.getStartDate().toString() + "'");
		}
		if (sample.getEndDate() != null) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.endDate = " + "'" + sample.getEndDate().toString() + "'");
		}
		// no fields specified to search by in the sample object
		if (!firstClauseAdded) {
			return result;
		}
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			System.out.println(query.toString());
			Query q = em.createQuery(query.toString());
			result = (List<Employee>) q.getResultList();
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}

	public List<Employee> getAll() {		
		return (List<Employee>) getAll(Employee.class);
	}

	public void save(Employee entity) {
		System.out.println("saving...");
		persist(entity);
		System.out.println("saved");
	}

	@Override
	public boolean update(Employee entity) {
		return super.refresh(entity);
	}

	@Override
	public boolean delete(Employee entity) {
		return remove(entity);		
	}

	@Override
	public Employee find(Employee sample) {
		Employee result = null;
		StringBuilder query = new StringBuilder();
		query.append("SELECT e FROM Employee e");
		boolean firstClauseAdded = false;
		if (sample.getFullName() != null && !sample.getFullName().isEmpty()) {
			query.append(" WHERE e.fullName = '" + sample.getFullName() + "'");
			firstClauseAdded = true;
		}
		if (sample.getDepartment() != null && !sample.getDepartment().getDeptName().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.department.deptName = '" + sample.getDepartment().getDeptName() + "'");
		}
		if (sample.getPhone() != null && !sample.getPhone().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.phone = '" + sample.getPhone() + "'");
		}
		if (sample.getPosition() != null && !sample.getPosition().isEmpty()) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.position = '" + sample.getPosition() + "'");
		}
		if (sample.getStartDate() != null) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.startDate = " + "'" + sample.getStartDate().toString() + "'");
		}
		if (sample.getEndDate() != null) {
			if (!firstClauseAdded) {
				query.append(" WHERE ");
				firstClauseAdded = true;
			} else {
				query.append(" AND ");
			}
			query.append("e.endDate = " + "'" + sample.getEndDate().toString() + "'");
		}
		// no fields specified to search by in the sample object
		if (!firstClauseAdded) {
			return result;
		}
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			System.out.println(query.toString());
			Query q = em.createQuery(query.toString());
			result = (Employee) q.getSingleResult();
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}


}

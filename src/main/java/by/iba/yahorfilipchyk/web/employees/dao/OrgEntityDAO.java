package by.iba.yahorfilipchyk.web.employees.dao;

import java.util.List;

import by.iba.yahorfilipchyk.web.employees.entities.OrgEntity;

public interface OrgEntityDAO<T extends OrgEntity> {

	/**
	 * 
	 * @param key Primary key for OrgEntity entity.
	 * @return OrgEntity entity appropriate to given key.
	 */
	T find(int key);
	
	/**
	 * Find an entity in datastore using not primary key but other fields.
	 * @param sample An entity to be compared with.
	 * @return Found entity or null.
	 */
	T find(T sample);
	
	/**
	 * 
	 * @param sample The sample object for comparing with to find common entities.
	 * @return List of OrgEntity objects.
	 */
	List<T> findCommon(T sample);
	
	/**
	 * 
	 * @return All OrgEntity entities from datastore.
	 */
	List<T> getAll();
	
	/**
	 * Persists corresponding entity to datastore.
	 * @param entity An entity to be saved in datastore.
	 */
	void save(T entity);
	
	/**
	 * Updates corresponding entity in datastore.
	 * @param entity An entity to be updated.
	 * @return true if entity was updated.
	 */
	boolean update(T entity);
	
	/**
	 * Deletes corresponding entity from datastore.
	 * @param entity An entity to be delete.
	 */
	boolean delete(T entity);
}

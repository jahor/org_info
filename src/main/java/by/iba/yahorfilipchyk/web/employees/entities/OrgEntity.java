package by.iba.yahorfilipchyk.web.employees.entities;

import java.io.Serializable;

public abstract class OrgEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2288856728337852948L;

	public abstract int getId();
}

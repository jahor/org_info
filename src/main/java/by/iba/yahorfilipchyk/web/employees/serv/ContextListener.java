package by.iba.yahorfilipchyk.web.employees.serv;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import by.iba.yahorfilipchyk.web.employees.management.ConnectionManager;
import by.iba.yahorfilipchyk.web.employees.management.InitParameters;
import by.iba.yahorfilipchyk.web.employees.management.ManagersFactory;

/**
 * 
 * @author Filipchyk_YP
 * Servlet context listener. Creates an application scoped data access manager when
 * context initializes (an application creation) 
 */
public class ContextListener implements ServletContextListener {

	private static ConnectionManager manager;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		manager.close();		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext ctx = event.getServletContext();
		String managerType = ctx.getInitParameter("manager_type");
		Map<String, String> params = new HashMap<>();
		switch (managerType) {
		case "type_jpa":
			params.put("unit", ctx.getInitParameter("jpa_unit"));
			break;
		case "type_jdbc":
			params.put("driver", ctx.getInitParameter("db_driver"));
			params.put("url", ctx.getInitParameter("db_url"));
			params.put("username", ctx.getInitParameter("db_username"));
			params.put("password", ctx.getInitParameter("db_password"));
			break;
		default:
			break;
		}
		manager = ManagersFactory.createManager(new InitParameters(managerType, params));
		event.getServletContext().setAttribute("manager", manager);
	}
	
	/**
	 * 
	 * @return Connection manager to be used in other parts of application
	 */
	public static ConnectionManager getManager() {
		return manager;
	}

}

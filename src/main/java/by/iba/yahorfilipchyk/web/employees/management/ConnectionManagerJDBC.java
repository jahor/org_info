package by.iba.yahorfilipchyk.web.employees.management;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.dao.jdbc.JdbcDepartmentDAO;
import by.iba.yahorfilipchyk.web.employees.dao.jdbc.JdbcEmployeeDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP Connection manager for accessing the database using
 *         jdbc.
 */
public class ConnectionManagerJDBC implements ConnectionManager {

	private final String DRIVER;
	private final String URL;
	private final String USERNAME;
	private final String PASSWORD;

	private GenericObjectPool connectionPool = null;
	private static DataSource dataSource = null;

	public ConnectionManagerJDBC(String driver, String url, String username, String password) {
		this.DRIVER = driver;
		this.URL = url;
		this.USERNAME = username;
		this.PASSWORD = password;
		setupDataSource();
	}
	
	public static Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	/**
	 * Closes connection pool to datastore.
	 */
	@Override
	public void close() {
		// TODO Auto-generated method stub
		System.out.println("Closing connection pool");
		try {
			this.connectionPool.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public OrgEntityDAO<Department> createDepartmentDAO() {
		return new JdbcDepartmentDAO();
	}

	@Override
	public OrgEntityDAO<Employee> createEmployeeDAO() {
		return new JdbcEmployeeDAO();
	}

	private void setupDataSource() {
		try {
			Class.forName(DRIVER);
			connectionPool = new GenericObjectPool();
			connectionPool.setMaxActive(10);

			ConnectionFactory cf = new DriverManagerConnectionFactory(URL,
					USERNAME, PASSWORD);

			PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf,
					connectionPool, null, "SELECT 1", false, true);
			dataSource = new PoolingDataSource(connectionPool);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}		
//		return new PoolingDataSource(connectionPool);
	}
}

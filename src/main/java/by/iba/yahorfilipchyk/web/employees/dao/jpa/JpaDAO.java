package by.iba.yahorfilipchyk.web.employees.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.openjpa.persistence.EntityExistsException;

import by.iba.yahorfilipchyk.web.employees.entities.OrgEntity;
import by.iba.yahorfilipchyk.web.employees.management.ConnectionManagerJPA;

/**
 * 
 * @author Filipchyk_YP
 * Parent class for all DAO classes accessing datastore using jpa.
 * Encapsulates EntityManagerFactory object and provides entity managers for accessing data.
 */
public class JpaDAO {	
	
	/**
	 * 
	 * @return Entity manager.
	 */
	public EntityManager getEntityManager() {
		return ConnectionManagerJPA.createEntityManager();
	}
	
	public List<? extends OrgEntity> getAll(Class<? extends OrgEntity> classOfEntity) {
		EntityManager em = getEntityManager();
		List<OrgEntity> result = null;
		em.getTransaction().begin();
		try {
			Query querry = em.createQuery("SELECT x FROM " + classOfEntity.getSimpleName() + " x");
			result = querry.getResultList();
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return result;
	}
	
	public boolean refresh(OrgEntity entity) {
		boolean updated = false;
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			OrgEntity fromStorage = em.find(entity.getClass(), entity.getId());
			System.out.println("from storage: " + fromStorage);
			if (fromStorage != null) {
				em.merge(entity);
			}
			em.getTransaction().commit();
			updated = true;
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return updated;
	}
	
	public void persist(OrgEntity entity) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			if (em.contains(entity)) {
				entity = em.merge(entity);
				em.refresh(entity);
			} else {
				em.persist(entity);
			}
			System.out.println("persisted");
			em.getTransaction().commit();
		} catch (EntityExistsException ex) {
			System.out.println("Entity exists");
		} catch (org.apache.openjpa.persistence.RollbackException ex) {
			System.out.println("Transaction was rolled back");
			ex.printStackTrace();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}
	
	public boolean remove(OrgEntity entity) {
		boolean removed = false;
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		try {
			System.out.println("from delete: " + entity);
			OrgEntity found = em.find(entity.getClass(), entity.getId()); 
			if (found != null) {
				em.remove(found);;
			} else {
				System.out.println("not found");
			}
			em.getTransaction().commit();
			removed = true;
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return removed;
	}
}

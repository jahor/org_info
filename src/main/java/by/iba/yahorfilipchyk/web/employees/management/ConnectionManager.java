package by.iba.yahorfilipchyk.web.employees.management;

import by.iba.yahorfilipchyk.web.employees.dao.OrgEntityDAO;
import by.iba.yahorfilipchyk.web.employees.entities.Department;
import by.iba.yahorfilipchyk.web.employees.entities.Employee;

/**
 * 
 * @author Filipchyk_YP
 * Connection manager abstract class. Provides methods for closing datastore and
 * getting DAO for accessing each entity class. 
 */
public interface ConnectionManager {

	/**
	 * Closes datastore.
	 */
	void close();
	
	/**
	 * 
	 * @return DAO object for accessing Department entities.
	 */
	OrgEntityDAO<Department> createDepartmentDAO();
	
	/**
	 * 
	 * @return DAO object for accessing Employee entities.
	 */
	OrgEntityDAO<Employee> createEmployeeDAO();

}

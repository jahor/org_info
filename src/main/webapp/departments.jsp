<%@page import="by.iba.yahorfilipchyk.web.employees.entities.Employee"%>
<%@page import="by.iba.yahorfilipchyk.web.employees.entities.Department"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="styles/style.css" />
        <link rel="stylesheet" href="styles/jquery-ui.css" />
	  	<script src="scripts/jquery-1.9.1.js"></script>
	  	<script src="scripts/jquery-ui.js"></script>
		<script src="scripts/ajax.js"></script>	
		<script src="scripts/dialogs_department.js"></script>
		<script src="scripts/dialogs_common.js"></script>
        <title>Departments</title>
    </head>
    <body>
    	<a id="home" href="/employees">Home</a>
        <header>
            <h1>Departments</h1>
        </header>
        <table>
            <tr>
                <th>Department</th>
                <th>Employees</th>
            </tr>
            <%
                List<Department> deps = (List<Department>) request.getAttribute("departments");
                int depsCount = 0;
                if (deps != null) {
                    depsCount = deps.size();
                }
                for (int i = 0; i < depsCount; i++) {
            %>
            <tr id="rec<%=i %>" onclick="highlight('rec<%=i %>')">
            	<td style="display: none"><%=deps.get(i).getId()%></td>
                <td><%=deps.get(i).getDeptName()%></td>
                <td><%
	                	List<Employee> empls = deps.get(i).getEmployees();
	                	int employeesCount = 0;
	                	if (empls != null) {
	                		employeesCount = empls.size();
	                		if (employeesCount > 0) {
                				out.print(empls.get(0).getFullName() == null ? "" : empls.get(0).getFullName());	
                			}
                		}           
                		for (int j = 1 ; j < employeesCount; j++) {
                			out.print(", " + (empls.get(j).getFullName() == null ? "" : empls.get(j).getFullName()));
                		}                	
                	%></td>
            </tr>
            <%
                }
            %>
        </table>
        
        <div id="dialog-form-add-dept" title="Add department">
  			<p class="validateTips"></p> 
  			<form id="add-dept-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="add" value="department" />
		    		<label for="deptName">Department name</label>
		    		<input type="text" name="dept_name" id="add_name_dept" class="text ui-widget-content ui-corner-all" />		   			
	  			</fieldset>
 			 </form>
		</div>
		
		<div id="dialog-form-search-dept" title="Search departments">
  			<p class="validateTips"></p> 
  			<form id="search-dept-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="search" value="department" />
		    		<label for="deptName">Department name</label>
		    		<input type="text" name="dept_name" id="search_name_dept" class="text ui-widget-content ui-corner-all" />		   			
	  			</fieldset>
 			 </form>
		</div>
		
		<div id="dialog-form-update-dept" title="Update department">
  			<p class="validateTips"></p> 
  			<form id="update-dept-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="update" value="department" />
	  				<input type="hidden" name="dept_id" id="dept_id" />
		    		<label for="deptName">Department name</label>
		    		<input type="text" name="dept_name" id="update_name_dept" class="text ui-widget-content ui-corner-all" />		   			
	  			</fieldset>
 			 </form>
		</div>
		
		<form id="delete-form-dept" action="main" method="POST">
	  		<fieldset>
	  			<input type="hidden" name="delete" value="department" />
		    	<input type="hidden" name="dept_name" id="del_dept_name" />
	  		</fieldset>
 		</form>
 		
 		<div id="dialog-confirm" title="Delete department?">
  			<p>
  				This item will be permanently deleted and cannot be recovered. Are you sure?
  			</p>
		</div>
 		
 		<div id="toolbar">
	        <button id="add_dept_btn">Add</button>
	        <button id="search_dept_btn">Search</button>
	        <button id="update_dept_btn">Update</button>
	        <button id="delete_dept_btn">Delete</button>
	        <button id="show-all_dept_btn">Show all</button>
        </div>
    </body>
</html>

var chosenRow = null;
var idToDelete = null;
var idToUpdate = null;

/**
 * Highlights chosen row.
 * @param id The row id.
 */
function highlight(id) {
	if (chosenRow != id && chosenRow != null) {
		highlight(chosenRow);
	}
	// getting all columns belonging current row
	var tds = document.getElementById(id).children;
	for ( var i = 0; i < tds.length; i++) {
		if (tds[i].style.background != 'white') {
			tds[i].style.background = 'white';
			chosenRow = id;
		} else {
			tds[i].style.background = '#dcdcdc';
			chosenRow = null;
		}
	}
}

/**
 * This function sends request to the server to get entity from DB asynchronously and registers
 * ajax listener function.
 * @param id Identifier of entity to be retrieved from the server.
 * @param whoToGet An entity type.
 * @param functionListener Function which will listen the server response.
 */
function compareWithDB(id, whoToGet, functionListener) {
	// Obtain an XMLHttpRequest instance
	var req = newXMLHttpRequest();

	// Set the handler function to receive callback notifications
	// from the request object
	var handlerFunction = getReadyStateHandler(req, functionListener);
	req.onreadystatechange = handlerFunction;

	// Open an HTTP POST connection to the shopping cart servlet.
	// Third parameter specifies request is asynchronous.
	req.open("POST", "main", true);

	// Specify that the body of the request contains form data
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	// Send form encoded data stating that I want to add the
	// specified item to the cart.
	req.send("get=" + whoToGet + "&id=" + id);
}

/**
 * Updates message on the dialog.
 * @param tips Tag that shows the message.
 * @param t Message.
 */
function updateTips(tips, t) {
	tips.text(t).addClass("ui-state-highlight");
	setTimeout(function() {
		tips.removeClass("ui-state-highlight", 1500);
	}, 500);
} 

/**
 * Checks the length of the field and shows error message if checking fails.
 * @param tips Tag that shows the message.
 * @param o Field.
 * @param n Field name.
 * @param min Minimum length.
 * @param max Maximum length.
 * @returns {Boolean}
 */
function checkLength(tips, o, n, min, max) {
	if (o.val().length > max || o.val().length < min) {
		o.addClass("ui-state-error");
		updateTips(tips, "Length of " + n + " must be between " + min + " and "
				+ max + ".");
		return false;
	} else {
		return true;
	}
}

/**
 * Checks the pattern of the field and shows error message if checking fails.
 * @param tips Tag that shows the message.
 * @param o Field.
 * @param regexp Regular expression.
 * @param n Error message.
 * @returns {Boolean}
 */
function checkRegexp(tips, o, regexp, n) {
	if (!(regexp.test(o.val()))) {
		o.addClass("ui-state-error");
		updateTips(tips, n);
		return false;
	} else {
		return true;
	}
}

function checkName(tipsClass, fieldId) {
	checkRegexp(
			$("." + tipsClass), 
			$("#" + fieldId), 
			/^[A-Z]([A-Za-z ])+$/, 
			"Full name may consist of a-z, A-Z and spaces, should start with uppercase letter."
	);
}
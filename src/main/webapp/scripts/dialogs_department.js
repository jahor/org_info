
/**
 * Gets an information about department entity from xml.
 * @param deptXML xml to be parsed.
 * @returns {Array}
 */
function getDepartmentFromXML(deptXML) {
	var department = new Array();
	var dept = deptXML.getElementsByTagName("department")[0];
	var id = "";
	if (dept.getElementsByTagName("id")[0].hasChildNodes()) {
		id = dept.getElementsByTagName("id")[0].firstChild.nodeValue;
	}
	department[0] = id;
	var deptName = "";
	if (dept.getElementsByTagName("deptName")[0].hasChildNodes()) {
		deptName = dept.getElementsByTagName("deptName")[0].firstChild.nodeValue;
	}
	department[1] = deptName;	
	return department;
}

/**
 * Function that listens for server response.
 * Sets update dialog fields.
 * @param emplXML XML returned by the server.
 */
function setUpdateDepartmentDialog(deptXML) {
	var dept = getDepartmentFromXML(deptXML);	
	$("#dept_id").val(dept[0]);
	
	$("#update_name_dept").val(dept[1]);
	
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	tds[1].innerText = dept[1];
	$("#dialog-form-update-dept").dialog("open");	
}

/**
 * Function that listens for server response.
 * Before submitting of update form checks for changes of entity in database.
 * @param emplXML XML returned by the server.
 */
function checkChangingsDept(deptXML) {
	var dept = getDepartmentFromXML(deptXML);
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	if (tds[1].innerText == dept[1]) {
		$("#update-dept-form").submit();
	} else {
		alert("Record was modified before!");
		tds[1].innerText = dept[1];
	}
}

/**
 * Function that listens for server response.
 * Before submitting of delete form checks for changes of entity in database.
 * @param emplXML XML returned by the server.
 */
function alertChangingsDept(deptXML) {
	var dept = getDepartmentFromXML(deptXML);	
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	if (tds[1].innerText == dept[1]) {
		$("#del_dept_name").val(tds[1].innerText);
		$("#delete-form-dept").submit();
	} else {
		alert("Record was modified before!");
		tds[1].innerText = dept[1];
	}
}

/**
 * Setting of jquery dialogs and buttons
 */
$(function() {
	var tips = $(".validateTips");
	var deptNameAdd = $("#add_name_dept"),
		deptNameSearch = $("#update_name_dept"),
		deptNameUpdate = $("#update_name_dept");	
	
	// dialog for adding new entity
	$("#dialog-form-add-dept").dialog({
		autoOpen : false,
		height : 300,
		width : 350,
		modal : true,
		buttons : {
			"Add department" : function() {
				var valid = true;
				$([]).add(deptNameAdd).removeClass("ui-state-error");		 
		        valid = valid && checkLength(tips, deptNameAdd, "department name", 2, 200);		 
		        valid = valid && checkRegexp(tips, deptNameAdd, /^[A-Z]([A-Za-z ])+$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		 
		        if (valid) {
		            document.getElementById("add-dept-form").submit();		    		
		            $(this).dialog("close");
		            $([]).add(deptNameAdd).val("").removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				$([]).add(deptNameAdd).val("").removeClass("ui-state-error");
			}
		}
	});	
	
	// dialog for searching entities
	$("#dialog-form-search-dept").dialog({
		autoOpen : false,
		height : 300,
		width : 350,
		modal : true,
		buttons : {
			"Search" : function() {
				var valid = true;
				$([]).add(deptNameSearch).removeClass("ui-state-error");
		 
		        valid = valid || checkLength(tips, deptNameSearch, "department name", 2, 200);
		 
		        valid = valid || checkRegexp(tips, deptNameSearch, /^[A-Z]([A-Za-z ])+$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		 
		        if (valid) {
		            document.getElementById("search-dept-form").submit();
		            $(this).dialog("close");
		            $([]).add(deptNameSearch).val("").removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				$([]).add(deptNameSearch).val("").removeClass("ui-state-error");
			}
		}
	});	
	
	// dialog for updating of chosen entity
	$("#dialog-form-update-dept").dialog({
		autoOpen : false,
		height : 300,
		width : 350,
		modal : true,
		buttons : {
			"Update" : function() {
				var valid = true;
				$([]).add(deptNameUpdate).removeClass("ui-state-error");
		 
		        valid = valid && checkLength(tips, deptNameUpdate, "department name", 2, 200);
		 
		        valid = valid && checkRegexp(tips, deptNameUpdate, /^[A-Z]([A-Za-z ])+$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		 
		        if (valid) {
		        	// sending of ajax request to the server to check for changes in DB
		        	compareWithDB(idToUpdate, "department", checkChangingsDept);
		            $(this).dialog("close");
		            $([]).add(deptNameUpdate).removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				$([]).add(deptNameUpdate).val("").removeClass("ui-state-error");
			}
		}
	});	
	
	$("#add_dept_btn").button().click(function() {
		$("#dialog-form-add-dept").dialog("open");
	});

	$("#search_dept_btn").button().click(function() {
		$("#dialog-form-search-dept").dialog("open");
	});

	$("#update_dept_btn").button().click(function() {
		if (chosenRow != null) {
			var row = document.getElementById(chosenRow);
			var tds = row.children;
			idToUpdate = tds[0].innerText;
			// sending ajax request to the server to check for updates in DB
			compareWithDB(idToUpdate, "department", setUpdateDepartmentDialog);
		} else {
			alert("Nothing chosen to update.");
		}
	});

	$("#delete_dept_btn").button().click(function() {
		if (chosenRow != null) {
			var row = document.getElementById(chosenRow);
			var tds = row.children;
			idToDelete = tds[0].innerText;
	    	$("#dialog-confirm").dialog("open");
		} else {
			alert("Nothing to delete.");
		}
	});

	// dialog for confirming deletation of entity
	$("#dialog-confirm").dialog({
		autoOpen : false,
		resizable : false,
		height : 250,
		width : 400,
		modal : true,
		buttons : {
			"Delete department" : function() {
				// sending ajax request to the server to check for updates in DB
				compareWithDB(idToDelete, "department", alertChangingsDept);
				$(this).dialog("close");
			},
			Cancel : function() {
				$(this).dialog("close");
			}
		}
	});

	$("#show-all_dept_btn").button().click(function() {
		window.location.href = "main?page=Departments";
	});
});
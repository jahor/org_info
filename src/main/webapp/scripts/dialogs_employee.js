
/**
 * Gets an information about employee entity from xml.
 * @param emplXML xml to be parsed.
 * @returns {Array}
 */
function getEmployeeFromXML(emplXML) {
	var employee = new Array();
	var empl = emplXML.getElementsByTagName("employee")[0];
	var id = "";
	if (empl.getElementsByTagName("id")[0].hasChildNodes()) {
		// getting element with index [1] because employee element contains two id
		// elements: id of department (0) and id of employee (1)
		id = empl.getElementsByTagName("id")[1].firstChild.nodeValue;
	}
	employee[0] = id;
	var fullName = "";
	if (empl.getElementsByTagName("fullName")[0].hasChildNodes()) {
		fullName = empl.getElementsByTagName("fullName")[0].firstChild.nodeValue;
	}
	employee[1] = fullName;
	var phone = "";
	if (empl.getElementsByTagName("phone")[0].hasChildNodes()) {
		phone = empl.getElementsByTagName("phone")[0].firstChild.nodeValue;
	}
	employee[2] = phone;
	var position = "";
	if (empl.getElementsByTagName("position")[0].hasChildNodes()) {
		position = empl.getElementsByTagName("position")[0].firstChild.nodeValue;
	}
	employee[3] = position;
	var deptName = "";
	if (empl.getElementsByTagName("department")[0]
						.getElementsByTagName("deptName")[0].hasChildNodes()) {
		deptName = empl.getElementsByTagName("department")[0]
						.getElementsByTagName("deptName")[0].firstChild.nodeValue;
	}
	employee[4] = deptName;	
	var startDate = "";
	if (empl.getElementsByTagName("startDate")[0].hasChildNodes()) {
		startDate = empl.getElementsByTagName("startDate")[0].firstChild.nodeValue;
	}
	employee[5] = startDate;
	var endDate = ""; 
	if (empl.getElementsByTagName("endDate")[0].hasChildNodes()) {
		endDate = empl.getElementsByTagName("endDate")[0].firstChild.nodeValue;
	}
	employee[6] = endDate;
	return employee;
}

/**
 * Function that listens for server response.
 * Sets update dialog fields.
 * @param emplXML XML returned by the server.
 */
function setUpdateEmployeeDialog(emplXML) {
	var empl = getEmployeeFromXML(emplXML);	
	// saving entity id in form
	$("#empl_id").val(empl[0]);
	
	$("#update_name").val(empl[1]);
	$("#update_phone").val(empl[2]);
	$("#update_position").val(empl[3]);
	$("#update_dept").val(empl[4]);
	$("#update_start_date").val(empl[5]);
	$("#update_end_date").val(empl[6]);
	
	// updating of record in the table
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	tds[1].innerText = empl[1];
	tds[2].innerText = empl[2];
	tds[3].innerText = empl[3];
	tds[4].innerText = empl[4];
	tds[5].innerText = empl[5];
	tds[6].innerText = empl[6];
	$("#dialog-form-update").dialog("open");	
}

/**
 * Function that listens for server response.
 * Before submitting of update form checks for changes of entity in database.
 * @param emplXML XML returned by the server.
 */
function checkChangingsEmpl(emplXML) {
	var empl = getEmployeeFromXML(emplXML);
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	if (tds[1].innerText == empl[1] &&
		tds[2].innerText == empl[2] &&
		tds[3].innerText == empl[3] &&
		tds[4].innerText == empl[4] &&
		tds[5].innerText == empl[5] &&
		tds[6].innerText == empl[6]
		) {
		// if entity wasn't changed the submit the form.
		$("#update-form").submit();
	} else {
		alert("Record was modified before!");
		tds[1].innerText = empl[1];
		tds[2].innerText = empl[2];
		tds[3].innerText = empl[3];
		tds[4].innerText = empl[4];
		tds[5].innerText = empl[5];
		tds[6].innerText = empl[6];
	}
}

/**
 * Function that listens for server response.
 * Before submitting of delete form checks for changes of entity in database.
 * @param emplXML XML returned by the server.
 */
function alertChangingsEmpl(emplXML) {
	var empl = getEmployeeFromXML(emplXML);	
	var row = document.getElementById(chosenRow);
	var tds = row.children;
	if (tds[1].innerText == empl[1] &&
		tds[2].innerText == empl[2] &&
		tds[3].innerText == empl[3] &&
		tds[4].innerText == empl[4] &&
		tds[5].innerText == empl[5] &&
		tds[6].innerText == empl[6]
		) {
		// if entity wasn't changed the submit the form.
		$("#delete_name").val(tds[1].innerText);
		$("#delete_phone").val(tds[2].innerText);
		$("#delete_position").val(tds[3].innerText);
		$("#delete_dept").val(tds[4].innerText);
		$("#delete_start_date").val(tds[5].innerText);
		$("#delete_end_date").val(tds[6].innerText);
		$("#delete-form").submit();
	} else {
		alert("Record was modified before!");
		tds[1].innerText = empl[1];
		tds[2].innerText = empl[2];
		tds[3].innerText = empl[3];
		tds[4].innerText = empl[4];
		tds[5].innerText = empl[5];
		tds[6].innerText = empl[6];
	}
}

/**
 * Setting of jquery dialogs and buttons
 */
$(function() {
	var tips = $(".validateTips");
	var nameAdd = $("#add_name"), 
		phoneAdd = $("#add_phone"), 
		positionAdd = $("#add_position"), 
		departmentAdd = $("#add_dept"), 
		startDateAdd = $("#add_start_date"),
		allFieldsAdd = $([]).add(nameAdd).add(phoneAdd).add(positionAdd).add(departmentAdd).add(startDateAdd), 
		nameSearch = $("#search_name");
		phoneSearch = $("#search_phone"), 
		positionSearch = $("#search_position"), 
		departmentSearch = $("#search_dept"), 
		startDateSearch = $("#search_start_date"),
		endDateSearch = $("#search_end_date"),
		allFieldsSearch = $([]).add(nameSearch).add(phoneSearch).add(positionSearch).add(departmentSearch).add(startDateSearch).add(endDateSearch),
		nameUpdate = $("#update_name"), 
		phoneUpdate = $("#update_phone"), 
		positionUpdate = $("#update_position"), 
		departmentUpdate = $("#update_dept"), 
		startDateUpdate = $("#update_start_date"),
		endDateUpdate = $("#update_end_date"),
		allFieldsUpdate = $([]).add(nameUpdate).add(phoneUpdate).add(positionUpdate).add(departmentUpdate).add(startDateUpdate).add(endDateUpdate);
	
	// dialog for adding new entity
	$("#dialog-form-add").dialog({
		autoOpen : false,
		height : 570,
		width : 350,
		modal : true,
		buttons : {
			"Add employee" : function() {
				var valid = true;
		        allFieldsAdd.removeClass("ui-state-error");
		 
		        valid = valid && checkLength(tips, nameAdd, "full name", 3, 200);
		        valid = valid && checkLength(tips, positionAdd, "position", 2, 50);
		        valid = valid && checkLength(tips, departmentAdd, "department", 2, 50);
		        valid = valid && checkLength(tips, startDateAdd, "start date", 10, 10);
		 
		        valid = valid && checkRegexp(tips, nameAdd, /^[A-Z]([A-Za-z ]){2,}$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		        valid = valid && checkRegexp(tips, positionAdd, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Position name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid && checkRegexp(tips, departmentAdd, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Department name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid && checkRegexp(tips, startDateAdd, /^\d\d\d\d-\d\d-\d\d$/, "Date should be equal to the pattern written above the appropriate inpu field.");		        
		 
		        if (valid) {
		            document.getElementById("add-form").submit();		    		
		            $(this).dialog("close");
		            allFieldsAdd.val("").removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				allFieldsAdd.val("").removeClass("ui-state-error");
			}
		}
	});
	
	// dialog for searching entities
	$("#dialog-form-search").dialog({
		autoOpen : false,
		height : 630,
		width : 350,
		modal : true,
		buttons : {
			"Search" : function() {
				var valid = true;
		        allFieldsSearch.removeClass("ui-state-error");
		 
		        valid = valid || checkLength(tips, nameSearch, "full name", 3, 200);
		        valid = valid || checkLength(tips, positionSearch, "position", 2, 50);
		        valid = valid || checkLength(tips, departmentSearch, "department", 2, 50);
		        valid = valid || checkLength(tips, startDateSearch, "start date", 10, 10);
		        valid = valid || checkLength(tips, endDateSearch, "end date", 10, 10);
		 
		        valid = valid || checkRegexp(tips, nameSearch, /^[A-Z]([A-Za-z ]){2,}$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		        valid = valid || checkRegexp(tips, positionSearch, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Position name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid || checkRegexp(tips, departmentSearch, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Department name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid || checkRegexp(tips, startDateSearch, /^\d\d\d\d-\d\d-\d\d$/, "Date should be equal to the pattern written above the appropriate inpu field.");
		        valid = valid || checkRegexp(tips, endDateSearch, /^\d\d\d\d-\d\d-\d\d$/, "Date should be equal to the pattern written above the appropriate inpu field.");	
		 
		        if (valid) {
		            document.getElementById("search-form").submit();
		            $(this).dialog("close");
		            allFieldsSearch.removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				allFieldsSearch.removeClass("ui-state-error");
			}
		}
	});
	
	// dialog for updating of chosen entity
	$("#dialog-form-update").dialog({
		autoOpen : false,
		height : 630,
		width : 350,
		modal : true,
		buttons : {
			"Update" : function() {
				var valid = true;
		        allFieldsSearch.removeClass("ui-state-error");
		 
		        valid = valid && checkLength(tips, nameUpdate, "full name", 3, 200);
		        valid = valid && checkLength(tips, positionUpdate, "position", 2, 50);
		        valid = valid && checkLength(tips, departmentUpdate, "department", 2, 50);
		        valid = valid && checkLength(tips, startDateUpdate, "start date", 10, 10);
		        if (endDateUpdate.val() != null && endDateUpdate.val() != "") {
		        	valid = valid && checkLength(tips, endDateUpdate, "start date", 10, 10);	
		        }
		 
		        valid = valid && checkRegexp(tips, nameUpdate, /^[A-Z]([A-Za-z ]){2,}$/, "Full name may consist of a-z, A-Z and spaces, should start with uppercase letter.");
		        valid = valid && checkRegexp(tips, positionUpdate, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Position name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid && checkRegexp(tips, departmentUpdate, /^[A-Z0-9]([A-Za-z 0-9]){1,}$/, "Department name may consist of a-z, A-Z and spaces, should start with uppercase letter or digit.");
		        valid = valid && checkRegexp(tips, startDateUpdate, /^\d\d\d\d-\d\d-\d\d$/, "Date should be equal to the pattern written above the appropriate input field.");
		        if (endDateUpdate.val() != null && endDateUpdate.val() != "") {
		        	valid = valid && checkRegexp(tips, endDateUpdate, /(^\d\d\d\d-\d\d-\d\d$)/, "Date should be equal to the pattern written above the appropriate input field.");	
		        }
		        
		        if (valid) {
		        	// sending of ajax request to the server to check for changes in DB
		        	compareWithDB(idToUpdate, "employee", checkChangingsEmpl);
		            $(this).dialog("close");
		            allFieldsUpdate.removeClass("ui-state-error");
		        }
			},
			Cancel : function() {
				$(this).dialog("close");
				allFieldsUpdate.val("").removeClass("ui-state-error");
			}
		}
	});
	
	$("#add").button().click(function() {
		$("#dialog-form-add").dialog("open");
	});

	$("#search").button().click(function() {
		$("#dialog-form-search").dialog("open");
	});

	$("#update").button().click(function() {
		if (chosenRow != null) {
			var row = document.getElementById(chosenRow);
			var tds = row.children;
			idToUpdate = tds[0].innerText;
			// sending ajax request to the server to check for updates in DB
			compareWithDB(idToUpdate, "employee", setUpdateEmployeeDialog);			
		} else {
			alert("Nothing chosen to update.");
		}
	});

	$("#delete").button().click(function() {
		if (chosenRow != null) {
			var row = document.getElementById(chosenRow);
			var tds = row.children;
			idToDelete = tds[0].innerText;
	    	$("#dialog-confirm").dialog("open");
		} else {
			alert("Nothing to delete.");
		}
	});	

	// dialog for confirming deletation of entity
	$("#dialog-confirm").dialog({
		autoOpen : false,
		resizable : false,
		height : 250,
		width : 400,
		modal : true,
		buttons : {
			"Delete employee" : function() {
				// sending ajax request to the server to check for updates in DB
				compareWithDB(idToDelete, "employee", alertChangingsEmpl);
				$(this).dialog("close");
			},
			Cancel : function() {
				$(this).dialog("close");
			}
		}
	});

	$("#show-all").button().click(function() {
		window.location.href = "main?page=Employees";
	});	
	
});
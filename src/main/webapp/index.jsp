<%-- 
    Document   : index
    Created on : Sep 19, 2013, 7:10:22 PM
    Author     : Yahor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="styles/style.css" />
        <title>Organization information</title>
    </head>
    <body>
        <header>
            <h1>Information about employees and departments</h1>
        </header>
        <form method="GET" action="main">
            <input type="submit" value="Employees" name="page" />
            <input type="submit" value="Departments" name="page" />
        </form>
    </body>
</html>

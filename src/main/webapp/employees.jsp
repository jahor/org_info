<%@page import="by.iba.yahorfilipchyk.web.employees.entities.Employee"%>
<%@page import="by.iba.yahorfilipchyk.web.employees.entities.Department"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="styles/style.css" />
        <link rel="stylesheet" href="styles/jquery-ui.css" />
	  	<script src="scripts/jquery-1.9.1.js"></script>
	  	<script src="scripts/jquery-ui.js"></script>
		<script src="scripts/ajax.js"></script>		
		<script src="scripts/dialogs_employee.js"></script>
		<script src="scripts/dialogs_common.js"></script>
        <title>Employees</title>
	</head>
    <body>
    	<a id="home" href="/employees">Home</a>
        <header>
            <h1>Employees</h1>
        </header>
        <table>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Position</th>
                <th>Department</th>
                <th>Start date</th>
                <th>End date</th>
            </tr>
            <%
                List<Employee> employees = (List<Employee>) request.getSession().getAttribute("employees");
                int employeesCount = 0;
                if (employees != null) {
                    employeesCount = employees.size();
                }
                for (int i = 0; i < employeesCount; i++) {
            %>
            <tr id="rec<%=i %>" onclick="highlight('rec<%=i %>')">
            	<td style="display: none"><%=employees.get(i).getId()%></td>
                <td><%=employees.get(i).getFullName()%></td>
                <td><%=employees.get(i).getPhone()%></td>
                <td><%=employees.get(i).getPosition()%></td>
                <td><%=employees.get(i).getDepartment() == null ? "" : employees.get(i).getDepartment().getDeptName() == null ? "" : employees.get(i).getDepartment().getDeptName()%></td>
                <td><%=employees.get(i).getStartDate()%></td>
                <td><%=employees.get(i).getEndDate() == null ? "" : employees.get(i).getEndDate()%></td>
            </tr>
            <%
                }
            %>
        </table>
        
        <div id="dialog-form-add" title="Add employee">
  			<p class="validateTips"></p> 
  			<form id="add-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="add" value="employee" />
		    		<label for="name">Full name</label>
		    		<input type="text" name="name" id="add_name" 
		    			class="text ui-widget-content ui-corner-all" 
		    			onblur="checkName('validateTips', 'add_name')" />
		   			<label for="phone">Phone</label>
		    		<input type="text" name="phone" id="add_phone" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="position">Position</label>
		    		<input type="text" name="position" id="add_position" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="department">Department</label>
		    		<!-- 
		    		<input type="text" name="dept" id="add_dept" value="" class="text ui-widget-content ui-corner-all" />
		    		-->
		    		<select name="dept" id="add_dept" class="text ui-widget-content ui-corner-all">
					<%
						List<Department> deps = (List<Department>) request.getSession().getAttribute("departments");
						int depsCount = 0;
						if (deps != null) {
							depsCount = deps.size();
						}
						for (int i = 0; i < depsCount; i++) {
					%>
					<option value="<%=deps.get(i).getDeptName() %>">
            				<%=deps.get(i).getDeptName() %>
            			</option>
            		 <% } %>
		    		</select><br />
		    		<label for="start_date">Start date (yyyy-mm-dd)</label>
		    		<input type="text" name="start_date" id="add_start_date" value="" class="text ui-widget-content ui-corner-all" />
	  			</fieldset>
 			 </form>
		</div>
		
		<div id="dialog-form-search" title="Search employees">
  			<p class="validateTips"></p> 
  			<form id="search-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="search" value="employee" />
		    		<label for="name">Full name</label>
		    		<input type="text" name="name" id="search_name" class="text ui-widget-content ui-corner-all" />
		   			<label for="phone">Phone</label>
		    		<input type="text" name="phone" id="search_phone" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="position">Position</label>
		    		<input type="text" name="position" id="search_position" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="department">Department</label>
		    		<!-- 
		    		<input type="text" name="dept" id="search_dept" value="" class="text ui-widget-content ui-corner-all" />
		    		-->
		    		<select name="dept" id="search_dept" class="text ui-widget-content ui-corner-all">
					<%
						for (int i = 0; i < depsCount; i++) {
					%>
					<option value="<%=deps.get(i).getDeptName() %>">
            				<%=deps.get(i).getDeptName() %>
            			</option>
            		 <% } %>
            		</select><br />
		    		<label for="start_date">Start date (yyyy-mm-dd)</label>
		    		<input type="text" name="start_date" id="search_start_date" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="end_date">End date (yyyy-mm-dd)</label>
		    		<input type="text" name="end_date" id="search_end_date" value="" class="text ui-widget-content ui-corner-all" />
	  			</fieldset>
 			 </form>
		</div>
		
		<form id="delete-form" action="main" method="POST">
	  		<fieldset>
	  			<input type="hidden" name="delete" value="employee" />
		    	<input type="hidden" name="name" id="delete_name" />
		   		<input type="hidden" name="phone" id="delete_phone" />
		    	<input type="hidden" name="position" id="delete_position" />
		    	<input type="hidden" name="dept" id="delete_dept" />
		    	<input type="hidden" name="start_date" id="delete_start_date" />
		    	<input type="hidden" name="end_date" id="delete_end_date" />
	  		</fieldset>
 		</form>
 		
 		<div id="dialog-confirm" title="Delete employee?">
  			<p>
  				This item will be permanently deleted and cannot be recovered. Are you sure?
  			</p>
		</div>
 			 
		<div id="dialog-form-update" title="Update employee">
  			<p class="validateTips"></p> 
  			<form id="update-form" action="main" method="POST">
	  			<fieldset>
	  				<input type="hidden" name="update" value="employee" />
	  				<input type="hidden" name="empl_id" id="empl_id" />
		    		<label for="name">Full name</label>
		    		<input type="text" name="name" id="update_name" class="text ui-widget-content ui-corner-all" />
		   			<label for="phone">Phone</label>
		    		<input type="text" name="phone" id="update_phone" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="position">Position</label>
		    		<input type="text" name="position" id="update_position" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="department">Department</label>
		    		<!-- 
		    		<input type="text" name="dept" id="update_dept" value="" class="text ui-widget-content ui-corner-all" />
		    		-->
		    		<select name="dept" id="update_dept" class="text ui-widget-content ui-corner-all">
					<%
						for (int i = 0; i < depsCount; i++) {
					%>
					<option value="<%=deps.get(i).getDeptName() %>">
            				<%=deps.get(i).getDeptName() %>
            			</option>
            		 <% } %>
            		</select><br />
		    		<label for="start_date">Start date</label>
		    		<input type="text" name="start_date" id="update_start_date" value="" class="text ui-widget-content ui-corner-all" />
		    		<label for="end_date">End date (yyyy-mm-dd)</label>
		    		<input type="text" name="end_date" id="update_end_date" value="" class="text ui-widget-content ui-corner-all" />
	  			</fieldset>
 			 </form>
		</div>		

		<div id="toolbar">
	        <button id="add">Add</button>
	        <button id="search">Search</button>
	        <button id="update">Update</button>
	        <button id="delete">Delete</button>
	        <button id="show-all">Show all</button>
        </div>
    </body>
</html>

